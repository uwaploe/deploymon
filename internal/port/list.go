package port

import serial "github.com/albenik/go-serial/v2"

func GetPortsList() ([]string, error) {
	return serial.GetPortsList()
}
