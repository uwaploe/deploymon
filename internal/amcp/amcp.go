// Package amcp implements the "client" side of the ABPR acoustic modem
// communication protocol
package amcp

import (
	"bytes"
	"context"
	"errors"
	"io"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/atmmsg"
	"github.com/golang/protobuf/proto"
)

const (
	GetDataRange   byte = 0x41
	GetData             = 0x42
	SessionDone         = 0x43
	ExecCommand         = 0x44
	GetStatus           = 0x45
	DataRangeResp       = 0xc1
	DataResp            = 0xc2
	ExecResponse        = 0xc4
	StatusResponse      = 0xc5
	AbortData           = 0xff
)

var ErrProtocol = errors.New("Protocol error")
var ErrData = errors.New("Data transfer error")

type Logger func(string, ...interface{})

var debugLog Logger = nil

// SetDebugLogger will enable or disable logging messages
func SetDebugLogger(logger Logger) {
	debugLog = logger
	atmmsg.TraceFunc = logger
}

type Client struct {
	rw io.ReadWriter
}

type clientOptions struct {
}

type Option struct {
	f func(*clientOptions)
}

func NewClient(rw io.ReadWriter, options ...Option) *Client {
	opts := clientOptions{}

	for _, opt := range options {
		opt.f(&opts)
	}

	return &Client{rw: rw}
}

func (c *Client) GetDataRange(ctx context.Context) (*abprdata.DataRange, error) {
	enc := atmmsg.NewEncoder(c.rw)
	enc.Write([]byte{GetDataRange})
	dr := abprdata.DataRange{}
	payload, _, err := atmmsg.ReadRawPacketCtx(ctx, c.rw)
	if err != nil {
		return &dr, err
	}
	if payload[0] != DataRangeResp {
		return &dr, ErrProtocol
	}
	err = proto.Unmarshal(payload[1:], &dr)
	return &dr, err
}

func (c *Client) RunCommand(ctx context.Context, req *abprdata.ExecReq) (*abprdata.ExecResp, error) {
	enc := atmmsg.NewEncoder(c.rw)
	buf := proto.NewBuffer([]byte{ExecCommand})
	buf.Marshal(req)
	enc.Write(buf.Bytes())
	resp := abprdata.ExecResp{}
	payload, _, err := atmmsg.ReadRawPacketCtx(ctx, c.rw)
	if err != nil {
		return &resp, err
	}
	if payload[0] != ExecResponse {
		return &resp, ErrProtocol
	}
	err = proto.Unmarshal(payload[1:], &resp)
	return &resp, err
}

func (c *Client) GetData(ctx context.Context, req *abprdata.DataReq,
	ch chan<- int) (*abprdata.Store, error) {
	enc := atmmsg.NewEncoder(c.rw)
	buf := proto.NewBuffer([]byte{GetData})
	buf.Marshal(req)
	enc.Write(buf.Bytes())

	var b bytes.Buffer
	recv := atmmsg.NewReceiver(c.rw)
	err := recv.Receive(ctx, &b, ch)
	if len(b.Bytes()) == 0 {
		return nil, ErrData
	}

	ds := abprdata.DeltaStore{}
	err = proto.Unmarshal(b.Bytes(), &ds)
	if err != nil {
		return nil, err
	}

	return ds.Expand(), nil
}

func (c *Client) AbortData() error {
	enc := atmmsg.NewEncoder(c.rw)
	enc.Write([]byte{AbortData})
	return nil
}

func (c *Client) SessionDone() error {
	enc := atmmsg.NewEncoder(c.rw)
	enc.Write([]byte{SessionDone})
	return nil
}

func (c *Client) GetStatus(ctx context.Context) (*abprdata.Status, error) {
	enc := atmmsg.NewEncoder(c.rw)
	buf := proto.NewBuffer([]byte{GetStatus})
	enc.Write(buf.Bytes())
	resp := abprdata.Status{}
	payload, _, err := atmmsg.ReadRawPacketCtx(ctx, c.rw)
	if err != nil {
		return &resp, err
	}
	if payload[0] != StatusResponse {
		return &resp, ErrProtocol
	}
	err = proto.Unmarshal(payload[1:], &resp)
	return &resp, err
}
