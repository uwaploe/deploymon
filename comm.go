package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/atm"
	"bitbucket.org/uwaploe/deploymon/internal/amcp"
	"bitbucket.org/uwaploe/deploymon/internal/port"
	"github.com/google/shlex"
	"github.com/wailsapp/wails"
	bolt "go.etcd.io/bbolt"
)

var ErrRestricted = errors.New("Restricted command")
var ErrNotConnected = errors.New("Not connected")
var ErrNoModem = errors.New("Modem not initialized")

func cmdMode(ctx context.Context, dev *atm.Device) error {
	_, err := dev.Exec(ctx, "AT")
	if err != nil {
		err = dev.Interrupt(ctx)
	}
	return err
}

// Wrapper struct for the Wails interface
type Modem struct {
	dev            *atm.Device
	conn           atm.Conn
	client         *amcp.Client
	setupTimeout   time.Duration
	cmdTimeout     time.Duration
	connectTimeout time.Duration
	rt             *wails.Runtime
	db             *bolt.DB
	log            *wails.CustomLogger
}

func NewModem() *Modem {
	m := &Modem{
		setupTimeout:   time.Second * 20,
		cmdTimeout:     time.Second * 20,
		connectTimeout: time.Second * 20,
	}

	atm.TraceFunc = log.Printf
	amcp.SetDebugLogger(log.Printf)
	return m
}

func (m *Modem) WailsInit(rt *wails.Runtime) error {
	m.rt = rt
	m.log = rt.Log.New("Deploymon")

	dir, err := m.rt.FileSystem.HomeDir()
	if err != nil {
		return err
	}

	dbdir := filepath.Join(dir, "data", "ABPR")
	os.MkdirAll(dbdir, 0755)

	const tsfmt = "20060102_150405"
	ts := time.Now().Format(tsfmt)
	dbPath := filepath.Join(dbdir, fmt.Sprintf("session_%s.db", ts))
	m.db, err = bolt.Open(dbPath, 0644, nil)
	if err != nil {
		return err
	}

	m.rt.Events.On("send", func(data ...interface{}) {
		eventStore(m.db, "send", data...)
	})
	m.rt.Events.On("receive", func(data ...interface{}) {
		eventStore(m.db, "receive", data...)
	})
	m.rt.Events.On("error", func(data ...interface{}) {
		eventStore(m.db, "error", data...)
	})
	m.rt.Events.Emit("mode", "command")
	return nil
}

func (m *Modem) WailsShutdown() {
	m.db.Close()
}

func (m *Modem) Setup(name string, isAtm88x bool) error {
	var (
		p   port.Port
		err error
	)

	var opts []atm.Option
	if isAtm88x {
		opts = append(opts, atm.ATM88x())
	}

	if strings.Contains(name, ":") {
		p, err = port.NetworkPort(name, rdTimeout)
	} else {
		p, err = port.SerialPort(name, 9600, rdTimeout)
	}

	if err != nil {
		return err
	}
	m.dev = atm.NewDevice(p, opts...)

	ctx, cancel := context.WithTimeout(context.Background(), m.setupTimeout)
	defer cancel()
	err = cmdMode(ctx, m.dev)
	if err != nil {
		m.log.Errorf("modem setup failed: %v", err)
		return err
	}

	return nil
}

func (m *Modem) IsSetup() bool {
	return m.dev != nil
}

// SendCmd sends a command to the Modem and returns the response along
// with any error that occurs.
func (m *Modem) SendCmd(cmd string) (string, error) {
	if m.dev == nil {
		return "", ErrNoModem
	}
	cmd = strings.ToLower(cmd)
	switch {
	case cmd == "ato":
		fallthrough
	case cmd == "atl":
		fallthrough
	case strings.HasPrefix(cmd, "aty"):
		fallthrough
	case strings.HasPrefix(cmd, "at$b"):
		fallthrough
	case strings.HasPrefix(cmd, "@opmode="):
		return "", ErrRestricted
	}
	m.rt.Events.Emit("send", cmd)
	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()
	resp, err := m.dev.Exec(ctx, cmd)
	m.rt.Events.Emit("receive", resp)
	return resp, err
}

// Connect initiates a connect with the remote acoustic modem at address addr
// and puts the Modem into online mode.
func (m *Modem) Connect(addr int) error {
	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()
	var err error
	m.conn, err = m.dev.Connect(ctx, addr)
	if err != nil {
		return err
	}

	m.client = amcp.NewClient(m.conn)
	m.rt.Events.Emit("mode", "online")

	return nil
}

func (m *Modem) IsOnline() bool {
	return m.dev.Online()
}

// Disconnect closes a remote connection and puts the Modem into command mode
func (m *Modem) Disconnect() error {
	if m.dev == nil {
		return ErrNoModem
	}

	if m.client == nil {
		m.rt.Events.Emit("mode", "command")
		return nil
	}
	m.conn.Close()
	m.client = nil

	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()
	err := cmdMode(ctx, m.dev)
	if err != nil {
		return err
	}
	m.rt.Events.Emit("mode", "command")
	m.dev.Exec(ctx, "ATH")

	return nil
}

// LoadFile reads a file of commands and sends each to the Modem in turn. The
// returned slice of atm.Trace structs contains a log of the Modem responses.
func (m *Modem) LoadFile() ([]atm.Trace, error) {
	name := m.rt.Dialog.SelectFile("Select modem command file", "*.txt")
	if name == "" {
		return nil, nil
	}

	f, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("Open %q failed: %v", name, err)
	}
	defer f.Close()

	ctx, cancel := context.WithTimeout(context.Background(), m.setupTimeout)
	defer cancel()
	return m.dev.Batch(ctx, f)
}

type TxSettings struct {
	Rate  int `json:"rate"`
	Power int `json:"power"`
}

func (tx TxSettings) String() string {
	return fmt.Sprintf("rate:%d power:%d", tx.Rate, tx.Power)
}

// Return the Modem's transmit settings
func (m *Modem) LocalTx() (TxSettings, error) {
	var (
		tx  TxSettings
		err error
	)

	if m.dev == nil {
		return tx, ErrNoModem
	}

	m.rt.Events.Emit("send", "LocalTx()")
	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()

	tx.Rate, err = m.dev.Register(ctx, 4)
	if err != nil {
		return tx, err
	}

	tx.Power, err = m.dev.Register(ctx, 6)
	m.rt.Events.Emit("receive", tx.String())
	return tx, err
}

// Return the transmit settings for a remote modem at address addr.
func (m *Modem) RemoteTx(addr int) (TxSettings, error) {
	var tx TxSettings

	if m.dev == nil {
		return tx, ErrNoModem
	}
	m.rt.Events.Emit("send", fmt.Sprintf("RemoteTx(%d)", addr))
	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()
	regs, err := m.dev.RemoteRegisters(ctx, addr)
	if err != nil {
		return tx, err
	}
	tx.Rate = int(regs[4])
	tx.Power = int(regs[6])
	m.rt.Events.Emit("receive", tx.String())
	return tx, nil
}

// Return the range in meters to the remote modem at addr
func (m *Modem) Range(addr int) (float64, error) {
	if m.dev == nil {
		return 0, ErrNoModem
	}
	m.rt.Events.Emit("send", fmt.Sprintf("Range(%d)", addr))
	ctx, cancel := context.WithTimeout(context.Background(), m.cmdTimeout)
	defer cancel()
	val, err := m.dev.Range(ctx, addr)
	if err != nil {
		return val, err
	}
	m.rt.Events.Emit("receive", val)
	return val, nil
}

func (m *Modem) GetStatus() (*abprdata.Status, error) {
	if m.dev == nil {
		return nil, ErrNoModem
	}

	if m.client == nil {
		return nil, ErrNotConnected
	}

	m.rt.Events.Emit("send", "GetStatus()")
	ctx, cancel := context.WithTimeout(context.Background(), m.setupTimeout)
	defer cancel()
	status, err := m.client.GetStatus(ctx)
	if err == nil {
		addStatus(m.db, "deployment-status", status)
	} else {
		m.rt.Events.Emit("error", err.Error())
	}
	m.rt.Events.Emit("receive", status.String())
	return status, err
}

type shellResponse struct {
	Stdout   string `json:"stdout"`
	Stderr   string `json:"stderr"`
	Exitcode int    `json:"exitcode"`
}

func (m *Modem) ShellCommand(cmdline string) (shellResponse, error) {
	if m.client == nil {
		return shellResponse{}, ErrNotConnected
	}
	cmds, err := shlex.Split(cmdline)
	if err != nil {
		return shellResponse{}, err
	}
	req := abprdata.ExecReq{
		Cmd:     cmds,
		Timeout: 10,
	}

	m.rt.Events.Emit("send", fmt.Sprintf("ShellCommand(%s)", req.Cmd))
	resp, err := m.client.RunCommand(context.Background(), &req)
	m.rt.Events.Emit("receive", resp.String())

	return shellResponse{
		Stdout:   string(resp.Stdout),
		Stderr:   string(resp.Stderr),
		Exitcode: int(resp.Exitcode)}, err
}

func (m *Modem) SessionDone() (string, error) {
	if m.client == nil {
		return "", ErrNotConnected
	}
	m.client.SessionDone()
	return "OK", nil
}
