package main

import (
	"encoding/json"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/protobuf/proto"
)

func addRecords(db *bolt.DB, bucket string, s *abprdata.Store) error {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		return err
	})
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		for _, rec := range s.Records {
			key := []byte(time.Unix(rec.Time, 0).UTC().Format(time.RFC3339))
			val, err := proto.Marshal(rec)
			if err != nil {
				return err
			}
			b := tx.Bucket([]byte(bucket))
			if err := b.Put(key, val); err != nil {
				return err
			}
		}
		return nil
	})

	return err
}

func addStatus(db *bolt.DB, bucket string, status *abprdata.Status) error {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		return err
	})
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		key := []byte(time.Unix(status.Time, 0).UTC().Format(time.RFC3339))
		val, err := proto.Marshal(status)
		if err != nil {
			return err
		}
		b := tx.Bucket([]byte(bucket))
		if err := b.Put(key, val); err != nil {
			return err
		}
		return nil
	})

	return err
}

type event struct {
	Name string          `json:"name"`
	Data json.RawMessage `json:"data"`
}

const evTimeFmt = "2006-01-02T15:04:05.000Z07:00"

func eventStore(db *bolt.DB, name string, data ...interface{}) error {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("events"))
		return err
	})

	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		var err error
		key := []byte(time.Now().UTC().Format(evTimeFmt))
		ev := event{Name: name}

		switch len(data) {
		case 0:
		case 1:
			ev.Data, err = json.Marshal(data[0])
		default:
			ev.Data, err = json.Marshal(data)
		}

		if err != nil {
			return err
		}

		val, err := json.Marshal(ev)
		if err != nil {
			return err
		}
		b := tx.Bucket([]byte("events"))
		return b.Put(key, val)
	})

	return err
}
