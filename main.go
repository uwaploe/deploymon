package main

import (
	"flag"
	"fmt"
	"time"

	_ "embed"

	"bitbucket.org/uwaploe/deploymon/internal/port"
	"github.com/wailsapp/wails"
)

const Usage = `Usage: deploymon [options] modem_device

Communicate with ABPR modem.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	remoteAddr                 = 2
	setupTimeout time.Duration = time.Second * 20
	rdTimeout    time.Duration = time.Millisecond * 2500
)

type Ports struct {
}

func (p *Ports) GetList() ([]string, error) {
	return port.GetPortsList()
}

//go:embed frontend/dist/app.js
var js string

//go:embed frontend/dist/app.css
var css string

func main() {

	app := wails.CreateApp(&wails.AppConfig{
		Width:     1024,
		Height:    768,
		Title:     fmt.Sprintf("ABPR Deployment Monitor: %s", Version),
		JS:        js,
		CSS:       css,
		Resizable: true,
		Colour:    "#131313",
	})

	app.Bind(&Ports{})
	app.Bind(NewModem())
	app.Run()
}
