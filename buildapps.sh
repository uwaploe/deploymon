#!/usr/bin/env bash

binname="$(jq -r -M '.binaryname' project.json)"
appname="$(jq -r -M '.name' project.json)"

set -e
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)

rm -rf build
mkdir -p build

# Build Windows executable
wails build -x windows/amd64 -p \
      -ldflags "-s -w -X main.Version=${vers} -X main.BuildDate=$(date +'%FT%T')"
cd build
archivedir="${binname}_${vers#v}_windows"
mkdir -p "$archivedir"
mv -v bitbucket.org/uwaploe/${binname}-windows-4.0-amd64.exe "$archivedir/${binname}.exe"
zip -r "${archivedir}.zip" "$archivedir"
rm -rf "$archivedir"
rm -rf bitbucket.org
cd -

# Build MacOS application
wails build -p \
      -ldflags "-s -w -X main.Version=${vers} -X main.BuildDate=$(date +'%FT%T')"
cd build
archivedir="${binname}_${vers#v}_macos"
mkdir -p "$archivedir"
mv -v "${appname}.app" "$archivedir/"
zip -r "${archivedir}.zip" "$archivedir"
rm -rf "$archivedir"
cd -
