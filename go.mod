module bitbucket.org/uwaploe/deploymon

go 1.16

require (
	bitbucket.org/uwaploe/abprdata v0.9.9
	bitbucket.org/uwaploe/atm v0.8.0
	bitbucket.org/uwaploe/atmmsg v0.6.3
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/albenik/go-serial v1.2.0
	github.com/albenik/go-serial/v2 v2.3.0
	github.com/fatih/color v1.10.0 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/leaanthony/slicer v1.5.0 // indirect
	github.com/pkg/browser v0.0.0-20210115035449-ce105d075bb4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/wailsapp/wails v1.16.9
	go.etcd.io/bbolt v1.3.5
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1 // indirect
	google.golang.org/protobuf v1.26.0
)
